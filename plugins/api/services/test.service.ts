
import { IResponse } from '~/plugins/api/IResponse';
import { ApiServiceClass } from '~/plugins/api/ApiServiceClass';

export class TestService extends ApiServiceClass {
  request (requestNumber: number): Promise<IResponse> {
    const request = this.createRequest('get', `/api/test?number=${requestNumber}`);

    return request;
  }
}
