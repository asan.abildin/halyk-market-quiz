import { NuxtAxiosInstance } from '@nuxtjs/axios';
import { AxiosError, AxiosResponse } from 'axios';

export interface IResponse {
  code: number,
  success: boolean,
  data?: any,
  error?: object,
};

export interface IApiServiceClass {
  [propName: string]: Function
}

export class ApiServiceClass {
  public axios: NuxtAxiosInstance;
  public mockAdapter: any;
  public mockModule: any;

  constructor (axios: NuxtAxiosInstance) {
    this.axios = axios;
  }

  public createRequest (method: 'get'|'post'|'delete'|'put', path: string, requestBody?: any): Promise<IResponse> {
    const request = this.axios[method](path, requestBody)
      // @ts-ignore
      .then((response: AxiosResponse) => {
        return {
          code: response.status,
          success: true,
          data: response.data.items || response.data,
        };
      })
      .catch((err: AxiosError) => {
        return {
          code: err.response?.data.code || err.code || err.response?.status,
          success: false,
          data: undefined,
        };
      });

    return request;
  }
}
