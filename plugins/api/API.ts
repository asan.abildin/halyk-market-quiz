import { NuxtAxiosInstance } from '@nuxtjs/axios';
import { TestService } from '~/plugins/api/services/test.service';

export interface IAPI {
  axios: NuxtAxiosInstance,
  test: TestService,
}

export default function (axios: NuxtAxiosInstance): IAPI {
  const API = {
    axios,
    test: new TestService(axios),
  };
  return API;
};
