import { mount } from '@vue/test-utils';
import Vue from 'vue';
// @ts-ignore для импортируемого компонента, нужен для того чтобы ts не ругался на него. ну не дружат ts и vue че поделать... а писать под каждый компонент свой интерфейс ну такое...
import HHeaderMenu from '~/components/layout/header/h-Header-Menu.vue';
import { IMenuItem } from '~/components/layout/type';

const list: IMenuItem[] = [
  {
    link: '/',
    label: 'label',
  },
  {
    link: '/label',
    label: 'label2',
  },
];

describe('HHeaderMenu', () => {
  it('is a Vue instance', () => {
    const wrapper = mount(HHeaderMenu, { propsData: { list }, stubs: { 'nuxt-link': true } });
    expect(wrapper.vm).toBeTruthy();
  });
  it('has passed props', () => {
    const wrapper = mount(HHeaderMenu, { propsData: { list }, stubs: { 'nuxt-link': true } });
    const component = wrapper.vm as HHeaderMenu;
    expect(component.list).toBe(list);
  });
  it('changes on prop change', async () => {
    const wrapper = mount(HHeaderMenu, { propsData: { list }, stubs: { 'nuxt-link': true } });
    const component = wrapper.vm as HHeaderMenu;
    wrapper.setProps({ list: [...list, ...list] });
    await Vue.nextTick();
    expect(component.list.length).toEqual(4);
  });
});
