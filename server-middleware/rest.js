import express from 'express';
import bodyParser from 'body-parser';

const app = express();

app.use(bodyParser.json());
app.get('/test', (req, res) => {
  if (req.query.number && !isNaN(req.query.number)) {
    if (parseInt(req.query.number) % 2) {
      res.json({ status: 'success' });
    } else {
      res.json({ status: 'error' });
    }
  } else {
    res.status(500);
    res.render('error', { error: 'err' });
  }
});

export default app;
